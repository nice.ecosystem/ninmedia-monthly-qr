package tv.ninmedia.monthlyqr;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.jms.annotation.EnableJms;

import java.lang.invoke.MethodHandles;

@EnableJms
@SpringBootApplication(scanBasePackages = {"tv.ninmedia.monthlyqr"})
public class Application {
    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public static void main(String[] args) {
        System.setProperty("server.servlet.context-path", "/monthly-qr");
        ApplicationContext context =  SpringApplication.run(Application.class, args);
    }
}
