package tv.ninmedia.monthlyqr;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.invoke.MethodHandles;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.joda.time.DateTime;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.messaging.handler.annotation.Headers;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;
import tv.ninmedia.monthlyqr.component.Mailer;

import javax.jms.JMSException;
import javax.jms.Session;

@Component
public class QrGenerator implements ApplicationRunner {

	private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());
	private Calendar c = GregorianCalendar.getInstance();

	private  int DATE_START_DAY = 1;
	private  int DATE_START_MONTH = 1;
	private  int DATE_START_YEAR = 2017;

	private  int TYPE_ID = 3;
	
	private  String[] titles = {};
	private  String[] channelIds = {};
	
	private  ApplicationContext context = null;
	private  Properties config = null;

	private  final SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;
	@Autowired
	private JmsTemplate jmsTemplate;
	@Autowired
	ThreadPoolTaskExecutor asyncExecutor;
	@Autowired
	Mailer mailer;

	public final static String QUEUE_GENERATOR = "queue-generator";

	@Override
	public void run(ApplicationArguments args) throws Exception {
		log.info("Starting service ....");
		//jmsTemplate.convertAndSend(QUEUE_GENERATOR, new JSONObject().toString());
	}

	@Scheduled(cron = "0 7 10 20 * *")
	public void test() {
		log.info("Crontab running ....");
		jmsTemplate.convertAndSend(QUEUE_GENERATOR, new JSONObject().toString());
	}

	@JmsListener(destination = QUEUE_GENERATOR)
	public void qrGeneratorListener(@Payload String jsonObjectString, @Headers MessageHeaders headers, Message message, Session session) {
		log.info("Received request to generate schedule");
		asyncExecutor.execute(new QrGeneratorWorker());
	}

	private class QrGeneratorWorker implements Runnable {
		private Calendar c;
		private  final XSSFWorkbook wb;
		private  final XSSFSheet sheet;
		private final String batchTag;
		private int rowNum = 0;

		public QrGeneratorWorker() {
			wb = new XSSFWorkbook();
			sheet = wb.createSheet("QR Codes");
			batchTag = "BATCH-"+(new SimpleDateFormat("yyyy-MM-dd_HH:mm:ss")).format(new Date());
		}

		private void resetCalendar() {
			c = Calendar.getInstance();
			c.set(Calendar.DAY_OF_MONTH,1);
			c.set(Calendar.HOUR_OF_DAY, 0);
			c.set(Calendar.MINUTE, 0);
			c.set(Calendar.SECOND, 0);
			c.add(Calendar.MONTH,1);
			log.info("Date: "+c.getTime());
		}

		@Override
		public void run() {
			resetCalendar();

			SimpleDateFormat ff = new SimpleDateFormat("yyyy-MM-dd");
			String wbFileName = "/tmp/qr-codes-"+ff.format(c.getTime())+".xlsx";

			Row rowHead = sheet.createRow(0);
			CellStyle style = wb.createCellStyle();
			style.setWrapText(true);
			style.setVerticalAlignment(XSSFCellStyle.VERTICAL_TOP);

			sheet.setColumnWidth(1, 4000);
			sheet.setColumnWidth(2, 7000);
			sheet.setColumnWidth(3, 10000);

			Cell cell = rowHead.createCell(0); cell.setCellStyle(style); cell.setCellValue("No.");
			cell = rowHead.createCell(1); cell.setCellStyle(style); cell.setCellValue("Date");
			cell = rowHead.createCell(2); cell.setCellStyle(style); cell.setCellValue("Program");
			cell = rowHead.createCell(3); cell.setCellStyle(style); cell.setCellValue("Code");
			cell = rowHead.createCell(4); cell.setCellStyle(style); cell.setCellValue("Point");

			MapSqlParameterSource params = new MapSqlParameterSource();
			String sql = " SELECT m.*, channel_name FROM promo_qr_monthly m JOIN tv_show_channel c ON (c.channel_id=m.channel_id) WHERE m.is_active=1 ORDER BY m.id ";
			List<Map<String,Object>> rows = jdbcTemplate.queryForList(sql, params);
			for (Map<String,Object> row : rows) {
				JSONObject jsonData = new JSONObject(row);
				generate(jsonData);
			}

			File wbFile = new File(wbFileName);

			try (FileOutputStream outputStream = new FileOutputStream(wbFile)) {
				wb.write(outputStream);
				wb.close();
				outputStream.close();
				send(wbFile);
			} catch (Exception e) {
				log.error("Error create excel file! "+e,e);
			}
		}

		private void send(File wbFile) throws Exception {
			resetCalendar();
			List<String> tos = new ArrayList<>();
			tos.add("indrasa@gmail.com");
			tos.add("rifki.sujudi@ninmedia.tv ");
			tos.add("carlos@ninmedia.tv ");
			tos.add("agung.andri@ninmedia.tv");
			tos.add("akmal@ninmedia.tv");
			List<File> attachments = new ArrayList<>();
			attachments.add(wbFile);
			String content = "Please check the attachment file!";
			String subject = "QR Generator - Period: "+(new DateTime(c.getTime()).toString("MMM yyyy"));
			mailer.send(tos,subject,content, attachments);
		}

		private void generate(JSONObject jsonData) {
			resetCalendar();

			int count = c.getActualMaximum(Calendar.DAY_OF_MONTH);

			log.info("Generating QR channel : "+jsonData.getString("channel_name"));

			SimpleDateFormat f2 = new SimpleDateFormat("d/M");

			for (int i=DATE_START_DAY;i<=count;i++) {
				rowNum++;
				Long channelId = jsonData.getLong("channel_id");
				int point = jsonData.getInt("point");
				c.set(Calendar.DAY_OF_MONTH, i);
				String dateStart = f.format(c.getTime());
				String title = jsonData.getString("channel_name") +" ("+f2.format(c.getTime())+")";

				c.set(Calendar.DAY_OF_MONTH, i+1);
				String dateEnd = f.format(c.getTime());
				log.info(title+" - start date: "+dateStart+" - "+dateEnd);

				insert(title, dateStart, dateEnd, rowNum, channelId, point);
			}
		}
		private void insert(String title, String d1, String d2, int rowNum, Long channelId, int point) {
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("title", title);
			params.put("d1", d1);
			params.put("d2", d2);
			params.put("tag", batchTag);
			params.put("point", point);
			params.put("typeId", TYPE_ID);

			String sql = " DELETE FROM promo_qr WHERE promo_name=:title ";
			jdbcTemplate.update(sql, params);

			KeyHolder holder = new GeneratedKeyHolder();
			SqlParameterSource paramMap = new MapSqlParameterSource(params);

			sql = " INSERT INTO promo_qr (promo_name,type_id,valid_from,valid_until,tag) VALUES (:title,:typeId,:d1,:d2,:tag) ";
			jdbcTemplate.update(sql, paramMap, holder);
			long promoId = holder.getKey().longValue();


			params.put("promoId", promoId);
			params.put("channelId", channelId);
			params.put("point", point);

			sql = " INSERT INTO promo_qr_channel (promo_id,channel_id) values (:promoId, :channelId) ";
			jdbcTemplate.update(sql, params);

			log.info("Promo ID: "+promoId);

			sql = " INSERT INTO promo_qr_type_poin (promo_id,point) VALUES (:promoId,:point) ";
			jdbcTemplate.update(sql, params);

			sql = " SELECT qr_code FROM promo_qr WHERE promo_id=:promoId ";
			String code = jdbcTemplate.queryForObject(sql, params, String.class);

			Row row = sheet.createRow(rowNum);
			row.createCell(0).setCellValue(rowNum+".");
			Cell dateCell = row.createCell(1);

			try {
				dateCell.setCellValue(f.parse(d1));
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			XSSFCellStyle dateCellStyle = wb.createCellStyle();
			short df = wb.createDataFormat().getFormat("dd-mmm-yy");
			dateCellStyle.setDataFormat(df);
			dateCell.setCellStyle(dateCellStyle);

			row.createCell(2).setCellValue(title);
			row.createCell(3).setCellValue(code);
			row.createCell(4).setCellValue(point);
		}
	}

}

