package tv.ninmedia.monthlyqr.controller;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

import static tv.ninmedia.monthlyqr.QrGenerator.QUEUE_GENERATOR;

@RestController
public class RestApi {
    @Autowired
    private JmsTemplate jmsTemplate;

    @RequestMapping(value = {"/"}, produces = "application/json;charset=UTF-8")
    public ResponseEntity<String> disburse(HttpServletRequest request) {
        JSONObject jsonResult = new JSONObject();
        jsonResult.put("success", true);

        jmsTemplate.convertAndSend(QUEUE_GENERATOR, new JSONObject().toString());

        return defaultReturn(jsonResult);
    }

    private ResponseEntity defaultReturn(JSONObject jsonResult) {
        HttpHeaders headers = new HttpHeaders();
        HttpStatus httpStatus = HttpStatus.OK;
        headers.setContentType(MediaType.APPLICATION_JSON_UTF8);
        return new ResponseEntity<String>(jsonResult.toString(), headers, httpStatus);
    }
}
