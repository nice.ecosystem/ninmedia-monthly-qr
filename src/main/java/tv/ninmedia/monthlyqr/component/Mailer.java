package tv.ninmedia.monthlyqr.component;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Component;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.lang.invoke.MethodHandles;
import java.util.List;

@Component
public class Mailer {
    @Autowired
    public JavaMailSender mailSender;

    @Value("${spring.mail.from}")
    private String from;

    private static final Logger log = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

    public void send(List<String> to, String subject, String htmlContent, List<File> attachments) throws Exception {
        MimeMessage mailMessage = mailSender.createMimeMessage();
        MimeMessageHelper helper = new MimeMessageHelper(mailMessage, true);

        String[] toList = new String[to.size()];
        toList = to.toArray(toList);

        helper.setTo(toList);
        helper.setFrom(from);
        helper.setCc(from);
        helper.setSubject(subject);
        helper.setText(htmlContent, true);

        if (attachments!=null) {
            for (File attachment : attachments ) {
                helper.addAttachment(attachment.getName(),attachment);
            }
        }

        boolean retry = false;
        int retryCount = 0;
        do {
            try {
                retry = false;
                mailSender.send(mailMessage);
                log.info("Email sent!");
            } catch (Exception e) {
                log.error("Error sending email: "+e,e);
                if (retryCount < 3) {
                    log.info("Retrying ...");
                    retry  = true;
                    retryCount++;
                }
            }
        } while(retry);

    }

}
